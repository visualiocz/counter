package cz.brezinajn.counter

// Uses androidx library
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

// Empty constructor
class MainActivity : AppCompatActivity() {

    // Value initializes on class instance initialization
    var counterValue = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Inflates layout and binds it to the activity
        setContentView(R.layout.activity_main)

        // Sets button onClick listeners. Uses Kotlin Android extensions - synthetic properties
        // Has to be done AFTER setContentView otherwise the app will crash on NPE
        buttonIncrease.setOnClickListener { increaseCounter() }
        buttonDecrease.setOnClickListener { decreaseCounter() }

        // Initializes view.
        updateView()
    }

    private fun updateView(){
        // Uses Kotlin Android Extensions - synthetic properties
        tvCounterValue.text = counterValue.toString()
    }

    private fun increaseCounter(){
        counterValue++
        updateView()
    }

    private fun decreaseCounter(){
        counterValue--
        updateView()
    }
}
